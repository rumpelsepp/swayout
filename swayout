#!/usr/bin/env bash

set -eu

# $1: serial
get_output_by_serial() {
    swaymsg -t get_outputs | jq -r ".[] | select(.serial == \""$1"\") | .name"
}

# $1: name
# $2: resolution
# $3: y position
# $4: x position
# $5: scaling
configure_output() {
    local cmd
    cmd="output "$1" resolution "$2" scale "$5" position "$3" "$4""
    swaymsg "$cmd"
}

# $1   : search string
# $2…$n: array
isin() {
    local search="$1"
    shift
    for v in "$@"; do
        if [[ "$v" == "$search" ]]; then
            return 0
        fi
    done
    return 1
}

# $1…$n: array
echo_array() {
    for v in "$@"; do
        echo "$v"
    done
}

usage() {
    echo "usage: $(basename "$0") [-lh] CONFIG"
    echo ""
    echo "options:"
    echo " -l   List available configs"
    echo " -h   Show this page and exit"
}

main() {
    local config
    local configs
    local userconfig="$HOME/.config/swayout/config.sh"

    if [[ ! -f "$userconfig" ]]; then
        echo "error: userconfig \"$userconfig\" does not exist"
        exit 1
    fi

    source "$userconfig"

    configs=($(declare -F | awk '/conf_.+/ {sub("conf_", "", $3); print $3}' | sort))
    while getopts "lh" arg; do
        case "$arg" in
            l)  echo_array "${configs[@]}" && exit 0;;
            h)  usage && exit 0;;
            *)  usage && exit 1;;
        esac
    done

    if [[ ! -n "${1-}" ]]; then
        echo "error: specify config; use \`-l\` to list them"
        exit 1
    fi

    if ! isin "$1" "${configs[@]}"; then
        echo "error: config \"$1\" does not exist"
        exit 1
    fi

    conf_"$1"
}

main "$@"

