set -l progname swayout

complete -c $progname -f
complete -c $progname -s l -d 'List available profiles'
complete -c $progname -s h -d 'Print a short help text and exit'
complete -c $progname -a "(swayout -l)"
