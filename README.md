# swayout

`swayout` is a simple bash script which helps organizing multiple [`sway`](https://swaywm.org/) output profiles.

## Usage

First, drop a config in `$HOME/swayout/config.sh`.
A profile is a bash function which needs to be prefixed with `conf_`.
Implement whatever you need for your usecase.

Available profiles can be listed with `-l`:

```
$ swayout -l
home
work
```

Apply a profile by passing it to `swayout`:

```
$ swayout home
```

## Config

Here is my config:

```
conf_home() {
    configure_output "$(get_output_by_serial 4RFMK58E0TYS)" "1920x1200" "0" "0" "1"
    configure_output "$(get_output_by_serial 0x00000000)" "2560x1440" "1920" "0" "1.2"
}

conf_work() {
    configure_output "$(get_output_by_serial RANDOMSERIAL)" "1920x1080" "0" "0" "1"
    configure_output "$(get_output_by_serial 0x00000000)" "2560x1440" "1920" "0" "1.2"
}

```

## Helpers

Helpers such as `get_output_by_serial` or `configure_output` are implemented in the [`swayout`](https://git.sr.ht/~rumpelsepp/swayout/tree/master/swayout) script and are automatically available in the config script.
For the available helpers just check the `swayout` file.
More helpers are welcome, just send a [PATCH](https://git-send-email.io/).

## Contributing

Just send emails to my [public inbox](https://lists.sr.ht/~rumpelsepp/public-inbox).
